var base_url = 'http://localhost:8080/api/';
var USERNAME = "tom";
var PASSWORD = "123";
var auth;
var userId = 0;
var test;
var indexOfQuery = 0;
var countRight = 0;

var mainDiv = document.querySelector('#contentDiv');

function renderAuthForm() {
  mainDiv.innerHTML = "<div class=\"card\">\n"
      + "  <div class=\"card-body\">\n"
      + "<h1>Вход</h1>"
      + "<input type=\"text\" id=\"usernameInput\" class=\"form-control\" aria-label=\"login\" placeholder='login' style='margin: 20px; padding: 10px' aria-describedby=\"inputGroup-sizing-default\">"
      + "<input type=\"password\" id=\"passwordInput\" class=\"form-control\" aria-label=\"password\" placeholder='password' style='margin: 20px; padding: 10px' aria-describedby=\"inputGroup-sizing-default\"> "
      + "<button type=\"button\" class=\"btn btn-primary btn-lg\" onclick='setUserData()'>Login</button>"
      + "  </div>\n"
      + "</div>";
}

function setUserData() {
  var usernameInput = document.querySelector('#usernameInput').value;
  var passwordInput = document.querySelector('#passwordInput').value;
  if (usernameInput && passwordInput) {
    USERNAME = usernameInput;
    PASSWORD = passwordInput;
    auth = "Basic " + btoa(usernameInput + ":" + passwordInput);
    $.ajax({
      type: 'GET',
      url: base_url + "user/getId",
      data: "login=" + usernameInput,
      datatype: "application/json",
      headers: {
        "Authorization": auth
      },
      success: function (data) {
        userId = data;
        renderMenu();
      },
      statusCode: {
        401: function () {
          alert("Ошибка! Неверные учетные данные");
          renderAuthForm();
        }
      }
    });
  } else {
    alert("Введите логин и пароль");
  }
}

function renderMenu() {
  mainDiv.innerHTML = "    <div class=\"btn-group-vertical\" style='width: -webkit-fill-available'>\n"
      + "      <button type=\"button\" class=\"btn btn-primary btn-lg\" style='width: -webkit-fill-available; margin: 5px' onclick=\"renderTestMenu()\">Тестирование</button>\n"
      + "      <button type=\"button\" class=\"btn btn-primary btn-lg\" style='width: -webkit-fill-available; margin: 5px' onclick=\"renderLearningMenu()\">Обучение</button>\n"
      + "      <button type=\"button\" class=\"btn btn-primary btn-lg\" style='width: -webkit-fill-available; margin: 5px' onclick=\"renderReports()\">Отчетность</button>\n"
      + "      <button type=\"button\" class=\"btn btn-primary btn-lg\" style='width: -webkit-fill-available; margin: 5px' onclick=\"(function() { location.reload(); })();\">Выход</button>\n"
      + "    </div>";
}

function renderTestMenu() {
  mainDiv.innerHTML = "";
  $.ajax({
    type: 'GET',
    url: base_url + "category/all",
    datatype: "application/json",
    async: false,
    headers: {
      "Authorization": auth
    },
    success: function (data) {
      mainDiv.innerHTML = "    <div class=\"card\" style='margin: 10px; padding: 10px'>\n"
          + "      <button type=\"button\" class=\"btn btn-success btn-sm\" style=\"margin: 5px; padding: 5px; width: 50px\" onclick=\"renderMenu()\">Back</button>\n"
          + "      <h4>Категория</h4>\n"
          + "      <select id=\"categorySelect\" style='margin: 10px; padding: 10px' onchange=\"loadTestItems()\"></select>\n"
          + "<div id='itemDiv' style='margin: 10px; padding: 10px'></div>"
          + "    </div>";
      let select = document.querySelector('#categorySelect');
      data.forEach(function (item) {
        select.innerHTML += "<option value='" + item.id + "'>" + item.name
            + "</option>";
      });
    }
  });
  loadTestItems();
}

function renderLearningMenu() {
  mainDiv.innerHTML = "";
  $.ajax({
    type: 'GET',
    url: base_url + "category/all",
    datatype: "application/json",
    async: false,
    headers: {
      "Authorization": auth
    },
    success: function (data) {
      mainDiv.innerHTML = "    <div class=\"card\" style='margin: 10px; padding: 10px'>\n"
          + "      <button type=\"button\" class=\"btn btn-success btn-sm\" style=\"margin: 5px; padding: 5px; width: 50px\" onclick=\"renderMenu()\">Back</button>\n"
          + "      <h4>Категория</h4>\n"
          + "      <select id=\"categorySelect\" style='margin: 10px; padding: 10px' onchange=\"loadLearningItems()\"></select>\n"
          + "<div id='itemDiv' style='margin: 10px; padding: 10px'></div>"
          + "    </div>";
      let select = document.querySelector('#categorySelect');
      data.forEach(function (item) {
        select.innerHTML += "<option value='" + item.id + "'>" + item.name
            + "</option>";
      });
    }
  });
}

function renderReports() {
  renderReport();
}

function loadLearningItems() {
  itemDiv = document.querySelector('#itemDiv');
  category_id = document.querySelector('#categorySelect').value;
  $.ajax({
    type: 'GET',
    url: base_url + "learningItem/category",
    data: 'categoryId=' + category_id,
    datatype: "application/json",
    async: false,
    headers: {
      "Authorization": auth
    },
    success: function (data) {
      itemDiv.innerHTML = "    <div class=\"card\" style='margin: 10px; padding: 10px'>\n"
          + "      <h4>Статья</h4>\n"
          + "      <select id=\"learningItemSelect\" style='margin: 10px; padding: 10px' onchange=\"loadLearningItemContent()\"></select>\n"
          + "     <div id='materialDiv' style='margin: 10px; padding: 10px'></div>"
          + "    </div>";
      let select = document.querySelector('#learningItemSelect');
      data.forEach(function (item) {
        select.innerHTML += "<option value='" + item.id + "'>" + item.name
            + "</option>";
      });
    }
  });
}

function loadLearningItemContent() {
  learning_id = document.querySelector('#learningItemSelect').value;
  $.ajax({
    type: 'GET',
    url: base_url + "learningItem/find",
    data: 'id=' + learning_id,
    datatype: "application/json",
    async: false,
    headers: {
      "Authorization": auth
    },
    success: function (data) {
      document.querySelector('#materialDiv').innerHTML =
          "<textarea readonly='readonly' style='height: 500px; width: 945px; margin: 10px'>"
          + data.item + "</textarea>"

    }
  });
}

function loadTestItems() {
  itemDiv = document.querySelector('#itemDiv');
  category_id = document.querySelector('#categorySelect').value;
  $.ajax({
    type: 'GET',
    url: base_url + "test/category",
    data: 'categoryId=' + category_id,
    datatype: "application/json",
    async: false,
    headers: {
      "Authorization": auth
    },
    success: function (data) {
      itemDiv.innerHTML = "    <div class=\"card\" style='margin: 10px; padding: 10px'>\n"
          + "      <h4>Тест</h4>\n"
          + "      <select id=\"testItemSelect\" style='margin: 10px; padding: 10px' onchange=\"selectTest()\"></select>\n"
          + "     <div id='startDiv' style='margin: 10px; padding: 10px'></div>"
          + "    </div>";
      let select = document.querySelector('#testItemSelect');
      data.forEach(function (item) {
        select.innerHTML += "<option value='" + item.id + "'>" + item.name
            + "</option>";
      });
    }
  });
  selectTest();
}

function selectTest() {
  let test_id = document.querySelector('#testItemSelect').value;
  $.ajax({
    type: 'GET',
    url: base_url + "test/load",
    data: 'id=' + test_id,
    datatype: "application/json",
    async: false,
    headers: {
      "Authorization": auth
    },
    success: function (data) {
      test = data;
      document.querySelector('#startDiv').innerHTML =
          "<button type=\"button\" class=\"btn btn-success btn-sm\" style=\"margin: 5px; padding: 5px; width: 50px\" onclick=\"startTest()\">Start</button>"
    }
  });
}

function startTest() {
  mainDiv.innerHTML = "";
  indexOfQuery = 0;
  countRight = 0;
  renderQuery();
}

function renderReport() {
  mainDiv.innerHTML = "      <button type=\"button\" class=\"btn btn-success btn-sm\" style=\"margin: 5px; padding: 5px; width: 50px\" onclick=\"renderMenu()\">Back</button>\n";
  $.ajax({
    type: 'GET',
    url: base_url + "result/report",
    data: 'userId=' + userId,
    datatype: "application/json",
    async: false,
    headers: {
      "Authorization": auth
    },
    success: function (data) {
      let table = document.createElement('table');
      table.className = "table table-bordered";
      table.innerHTML="<tr><td colspan='3'>" + data.fio + "</td><td>Успеваемость</td><td>" + data.averagePercent + "</td></tr>";
      table.innerHTML+="<tr><td>Категория</td><td>Тест</td><td>Результат худший</td><td>Результат лучший</td><td>Результат средний</td></tr>";

      data.results.forEach(function (item) {
        let rowUser = document.createElement('tr');
        rowUser.innerHTML +=
            "<td>" + item.category + "</td>"
            + "<td>" + item.name + "</td>"
            + "<td>" + item.worstPercent.toFixed(2) + "%</td>"
        + "<td>" + item.bestPercent.toFixed(2) + "%</td>"
        + "<td>" + item.averagePercent.toFixed(2) + "%</td>";
        table.appendChild(rowUser);
      });
      mainDiv.appendChild(table);
    }
  });

}

function renderQuery() {
  if (indexOfQuery >= test.queryList.length) {
    doneTest();
    return;
  }
  let currentQuery = test.queryList[indexOfQuery];
  mainDiv.innerHTML = "    <h3> Вопрос</h3>\n"
      + "    <p>" + currentQuery.query + "</p>\n"
      + "    <br/>";
  if (currentQuery.answer1 != "") {
    mainDiv.innerHTML += " <label>1)"
        + currentQuery.answer1 + "</label><br/>";
  }
  if (currentQuery.answer2 != "") {
    mainDiv.innerHTML += " <label>2)"
        + currentQuery.answer2 + "</label><br/>";
  }
  if (currentQuery.answer3 != "") {
    mainDiv.innerHTML += " <label>3)"
        + currentQuery.answer3 + "</label><br/>";
  }
  if (currentQuery.answer4 != "") {
    mainDiv.innerHTML += " <label>4)"
        + currentQuery.answer4 + "</label><br/>";
  }
  mainDiv.innerHTML += "<br>";
  mainDiv.innerHTML += "<h4>Ваш ответ</h4>"
  mainDiv.innerHTML += "    <select id = \"yourAnswer\">\n"
      + "      <option value=\"1\">1</option>\n"
      + "      <option value=\"2\">2</option>\n"
      + "      <option value=\"3\">3</option>\n"
      + "      <option value=\"4\">4</option>\n"
      + "    </select>\n"
      + "    <button type=\"button\" class=\"btn btn-info btn-lg\" onclick='nextQuery()'>Ответить</button>"
}

function nextQuery() {
  let currentQuery = test.queryList[indexOfQuery];
  let yourAnswer = document.querySelector('#yourAnswer').value;
  if (currentQuery.numOfRight == yourAnswer) {
    countRight++;
  }
  indexOfQuery++;
  renderQuery();
}

function doneTest() {
  mainDiv.innerHTML = "<h1>Результат</h1>"
  mainDiv.innerHTML += "<h3>" + ((countRight / test.queryList.length)
      * 100).toFixed(2) + " %</h3>"
  mainDiv.innerHTML += "      <button type=\"button\" class=\"btn btn-success btn-sm\" style=\"margin: 5px; padding: 5px; width: 50px\" onclick=\"renderMenu()\">Menu</button>\n";
  $.ajax({
    type: 'GET',
    url: base_url + "result/save",
    data: 'userId=' + userId + '&testId=' + test.id + "&countRight="
        + countRight,
    datatype: "application/json",
    async: false,
    headers: {
      "Authorization": auth
    },
    success: function (data) {
      test = null;
      indexOfQuery = 0;
    }
  });
}