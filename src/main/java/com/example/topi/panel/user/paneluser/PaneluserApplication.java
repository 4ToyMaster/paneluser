package com.example.topi.panel.user.paneluser;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PaneluserApplication {

	public static void main(String[] args) {
		SpringApplication.run(PaneluserApplication.class, args);
	}

}

